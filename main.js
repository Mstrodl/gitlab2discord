const config = require("./config.json");

const express = require("express");
const fetch = require("node-fetch");
const app = express();
const bodyParser = require("body-parser");

const handlers = {
  push: require("./handlers/push"),
  issue: require("./handlers/issue"),
  merge_request: require("./handlers/mergeRequest"),
  // lmao do you think this is fedi or something
  note: require("./handlers/note"),
  pipeline: require("./handlers/pipeline")
};

app.get("/gitlab2discord", function(req, res) {
  res
    .status(400)
    .send("Cannot use GET for gitlab2discord, please resend using POST.");
});

app.use(bodyParser.json(), bodyParser.urlencoded({ extended: true }));
app.post("/gitlab2discord", async function(req, res) {
  if (!req.query.cid || !req.query.token) {
    res
      .status(400)
      .send(
        "Token or Channel ID not found. Usage: /gitlab2discord?cid=<channel id>&token=<token>"
      );
    return;
  }
  const handler = handlers[req.body.object_kind];
  if (!handler) {
    console.warn(
      `Don't know how to handle object of type: ${req.body.object_kind}`
    );
    return res.status(204).send();
  }
  const embed = handler(req.body);
  if (!embed) {
    return;
  }

  await fetch(`https://discordapp.com/api/webhooks/${args.cid}/${args.token}`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({ embeds: [embed] })
  });
});

app.listen(config.port, function() {
  console.log(
    `gitlab2discord running on port ${config.port}, at /gitlab2discord.`
  );
});

module.exports.handlers = handlers;
