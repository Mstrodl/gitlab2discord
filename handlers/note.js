const config = require("../config.json");

const resourceTypes = {
  Commit(data) {
    return `commit ${data.commit.id.substring(0, 6)}`;
  },
  MergeRequest(data) {
    return `merge request #${data.merge_request.iid} __${data.merge_request.title}__`;
  },
  Issue(data) {
    return `issue #${data.issue.iid} __${data.issue.title}__`;
  },
  Snippet(data) {
    return `snippet #${data.snippet.id} __${data.snippet.title}__`;
  }
};

module.exports = function noteHandler(data) {
  const comment = data.object_attributes;
  return {
    title: `New comment on ${resourceTypes[comment.noteable_type](data)} of ${
      data.project.name
    }`,
    url: comment.url,
    description: comment.note,
    author: {
      name: `${data.user.name} (${data.user.username})`,
      icon_url: data.user.avatar_url,
      url: `${config.gitlabBase}/${data.user.username}`
    },
    color: 0x80c0ff,
    timestamp: new Date().toISOString()
  };
};
