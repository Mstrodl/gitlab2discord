const config = require("../config.json");

// https://github.com/discordjs/discord.js/blob/1352bff2fd969092b48368ebc1f05a080572f827/src/util/Util.js#L559-L561
function cleanCodeBlockContent(text) {
  return text.replace("```", "`\u200b``");
}

module.exports = function pushHandler(data) {
  const commits = data.commits.map(
    commit =>
      `[\`${commit.id.substring(0, 6)}\`](${commit.url}) - ${commit.message}`
  );

  const branch = data.ref.substring(11);

  const latestCommit = data.commits[data.commits.length - 1];

  // Unsure why this is different
  if (!data.user) {
    data.user = {
      username: data.user_username,
      name: data.user_name,
      avatar_url: data.user_avatar
    };
  }

  return {
    title: `New commit to \`${branch}\` on \`${data.project.namespace}/${data.project.name}\``,
    url:
      (latestCommit && latestCommit.url) ||
      `${data.project.web_url}/tree/${branch}`,
    description: `\`\`\`
${cleanCodeBlockContent(commits.join("\n\n"))}
\`\`\``,
    author: {
      name: `${data.user.name} (${data.user.username})`,
      icon_url: data.user.avatar_url,
      url: `${config.gitlabBase}/${data.user.username}`
    },
    color: 0x80ff80,
    footer: {
      text: `Commits this push: ${data.total_commits_count}`
    },
    timestamp: new Date().toISOString()
  };
};
