const config = require("../config.json");

const actions = {
  open: (issue, projectName) =>
    `New issue for \`${projectName}\` - __${issue.title}__`,
  close: (issue, projectName) =>
    `Issue for \`${projectName}\` - __${issue.title}__ closed`,
  update: (issue, projectName) =>
    `Issue for \`${projectName}\` - __${issue.title}__ updated`
};

module.exports = function(data) {
  const issue = data.object_attributes;

  return {
    title: (actions[issue.action] || actions.update)(
      issue,
      `${data.project.namespace}/${data.project.name}`
    ),
    url: `${data.project.web_url}/issues/${issue.iid}`,
    description: issue.description
      ? issue.description
      : "*No description provided*",
    author: {
      name: `${data.user.name} (${data.user.username})`,
      icon_url: data.user.avatar_url,
      url: `${config.gitlabBase}/${data.user.username}`
    },
    fields: [{ name: "State", value: issue.state }],
    color: 0xff8080,
    timestamp: new Date().toISOString()
  };
};
