const config = require("../config.json");

const colors = {
  pending: 0xc0c0c0,
  running: 0xffc080,
  success: 0x80ffc0,
  failed: 0xff80c0
};

const emoji = {
  pending: "\uD83D\uDEA7",
  running: "\uD83D\uDEA7",
  success: "\u2705",
  failed: "\u274c"
};

const messages = {
  pending: "Pending...",
  running: "Running...",
  success: "Passed.",
  failed: "Failed."
};

module.exports = function(data) {
  const pipeline = data.object_attributes;
  if (
    pipeline.status &&
    (pipeline.status == "pending" || pipeline.status == "running") &&
    config.disablePipelineExtras == true
  )
    return;

  return {
    title: `${
      emoji[pipeline.status] ? emoji[pipeline.status] : "\u2753"
    } Pipeline for \`${data.commit.id.substring(0, 6)}\` on ${
      data.project.name
    }`,
    url: data.commit.url + "/pipelines",
    description: `${
      messages[pipeline.status]
        ? messages[pipeline.status]
        : "!!! Unknown Status Passed !!!"
    } ${pipeline.duration != null ? `Took ${pipeline.duration} seconds.` : ""}`,
    color: colors[pipeline.status] || colors.pending,
    timestamp: new Date().toISOString()
  };
};
